const sql = require("./db");
var moment = require('moment');

const Room = function (room) {
  this.RoomID = room.RoomID;
  this.Roomname = room.Roomname;
  this.Build = room.Build;
  this.Capacity = room.Capacity;
};

Room.SelectBuild = (result) => {
  sql.query(`SELECT Build FROM projectroom.Room group by Build;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    result(null, res);
  });
};

Room.SelectTable = (selecttable, result) => {
  sql.query(`Select Details.Dates,Details.status,Timeslot.TimeId
    from Details,Timeslot,Room
    where Details.TimeId = Timeslot.TimeId
    and Details.RoomID = Room.RoomID
    and Details.Dates between curdate() and curdate()+7
    and Room.Build = ?
    and Room.Roomname = ?
    order by Details.Dates,Timeslot.TimeId`, [selecttable.Build, selecttable.Roomname], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    for (var i = 0; i < res.length; i++) {
      res[i].Dates = moment(res[i].Dates).add(1, 'days').utc().format('YYYY-MM-DD')
    }

    let count = 0;
    let data = [];
    let array = [];
    res.forEach((arritem, arrindex) => {
    
      if (count <= 22) {
        array.push(arritem.status)
          
        count++;
      }
      if (count === 22) {
          data.push({
            Dates : arritem.Dates,
            t1 : array[0],
            t2 : array[1],
            t3 : array[2],
            t4 : array[3],
            t5 : array[4],
            t6 : array[5],
            t7 : array[6],
            t8 : array[7],
            t9 : array[8],
            t10 : array[9],
            t11 : array[10],
            t12 : array[11],
            t13 : array[12],
            t14 : array[13],
            t15 : array[14],
            t16 : array[15],
            t17 : array[16],
            t18 : array[17],
            t19 : array[18],
            t20 : array[19],
            t21 : array[20],
            t22 : array[21],
          })
          count = 0;
          array = [];
      }
    });
   
    result(null,  data);
  });
}

Room.SelectRoom = (selectroom, result) => {
  sql.query(`SELECT Room.Roomname FROM projectroom.Room where Room.Build = ? group by Roomname;`, selectroom.Build, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    result(null, res);
  });
};

module.exports = Room;
