const sql = require("./db");
var moment = require('moment');

const Details = function (details) {
  this.DetailId = details.DetailId;
  this.TimeStart = details.TimeStart;
  this.TimeEnd = details.TimeEnd;
  this.Dates = details.Dates;
  this.RoomID = details.RoomID;
  this.BookingId = details.BookingId
  this.Status = details.Status
  this.Build = details.Build
  this.Capacity = details.Capacity
  this.Roomname = details.Roomname
  this.Subject = details.Subject

};


// Details.search = (searchDetail, result) => {
//   // searchDetail.Dates = searchDetail.Dates.split("/").reverse().join("-");
//   sql.query(`SELECT Room.Roomname , Room.Build ,min(Details.Dates)  
//    FROM projectroom.Details ,projectroom.Timeslot ,projectroom.Room 
//   WHERE  Details.TimeId = Timeslot.TimeId  and Details.RoomID = Room.RoomID  
//   and BookingId is  null and TimeStart BETWEEN ? 
//   AND ADDTIME(?,'00:-30:00') AND  TimeEnd BETWEEN ADDTIME(?,'00:30:00') AND ? 
//   And Status = ? And  Room.Build= ? 
//   And  Room.Capacity = ? 
//   And  Details.Dates = ?
//   group by Room.Roomname, Room.Build;
  
//   SELECT Room.Roomname , Room.Build , Timeslot.TimeStart , Timeslot.TimeEnd ,Details.Dates
//    FROM projectroom.Details ,projectroom.Timeslot ,projectroom.Room 
//   WHERE  Details.TimeId = Timeslot.TimeId  and Details.RoomID = Room.RoomID  
//   and BookingId is  null and TimeStart BETWEEN ? 
//   AND ADDTIME(?,'00:-30:00') AND  TimeEnd BETWEEN ADDTIME(?,'00:30:00') AND ?
//   And Status = ? And  Room.Build= ? 
//   And  Room.Capacity = ? 
//   And  Details.Dates = ?
//   order by Room.RoomID,TimeStart`, [searchDetail.TimeStart, searchDetail.TimeEnd, searchDetail.TimeStart,
//   searchDetail.TimeEnd, searchDetail.Status, searchDetail.Build, searchDetail.Capacity, searchDetail.Dates,
//   searchDetail.TimeStart, searchDetail.TimeEnd, searchDetail.TimeStart,
//   searchDetail.TimeEnd, searchDetail.Status, searchDetail.Build, searchDetail.Capacity, searchDetail.Dates],
//     (err, res) => {
//       if (err) {
//         console.log("error: ", err);
//         result(err, null);
//         return;
//       }
//       // for (var i = 0; i < res[0].length; i++) {
//       //   res[0][i].Dates = moment(res[0][i].min(Details.Dates)).add(1, 'days').utc().format('YYYY-MM-DD')
//       // }
//         for (var i = 0; i < res[1].length; i++) {
//           res[1][i].Dates = moment(res[1][i].Dates).add(1, 'days').utc().format('YYYY-MM-DD')
//         }
//       // }
//       // res[0].Roomname = res[0][0].Roomname
//       console.log(" show",res[0]);
//       result(null, res);

//     });
// }

Details.search = (searchDetail, result) => {
  // searchDetail.Dates = searchDetail.Dates.split("/").reverse().join("-");
  sql.query(`SELECT Room.Roomname , Room.Build ,Details.Dates, Timeslot.TimeStart , Timeslot.TimeEnd
   FROM projectroom.Details ,projectroom.Timeslot ,projectroom.Room 
  WHERE  Details.TimeId = Timeslot.TimeId  and Details.RoomID = Room.RoomID  
  and BookingId is  null and TimeStart BETWEEN ? 
  AND ADDTIME(?,'00:-30:00') AND  TimeEnd BETWEEN ADDTIME(?,'00:30:00') AND ?
  And Status = ? And  Room.Build= ? 
  And  Room.Capacity = ? 
  And  Details.Dates = ?
  order by Room.RoomID,TimeStart`, [searchDetail.TimeStart, searchDetail.TimeEnd, searchDetail.TimeStart,
  searchDetail.TimeEnd, searchDetail.Status, searchDetail.Build, searchDetail.Capacity, searchDetail.Dates,
],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
      // for (var i = 0; i < res[0].length; i++) {
      //   res[0][i].Dates = moment(res[0][i].min(Details.Dates)).add(1, 'days').utc().format('YYYY-MM-DD')
      // }
        for (var i = 0; i < res.length; i++) {
          res[i].Dates = moment(res[i].Dates).add(1, 'days').utc().format('YYYY-MM-DD')
        }
      // }
      let count =0;
      let data = [];
      res.forEach((arritem, arrindex) => {
          // เมื่อเข้ามาครั้งแรก
          if(arrindex === 0){
              data.push({
                  Roomname: arritem.Roomname,
                  Build: arritem.Build,
                  Dates: arritem.Dates,
                  Time : [
                      {TimeStart: arritem.TimeStart, TimeEnd: arritem.TimeEnd}
                  ]
              })
              
          } else {
              count =0;
              // เช็คว่ามีค่าเหมือนกันหรือไม่ เหมือนเข้า if ไม่เหมือนเข้า else
              data.forEach((dataitem, dataindex) => {
                  count++
                 if(dataitem.Roomname === arritem.Roomname && dataitem.Build === arritem.Build && dataitem.Dates === arritem.Dates) {
                      data[dataindex].Time.push({
                          TimeStart: arritem.TimeStart, TimeEnd: arritem.TimeEnd
                      })
                 } if(dataitem.Roomname != arritem.Roomname && count === data.length) {
                      data.push({
                          Roomname: arritem.Roomname,
                          Build: arritem.Build,
                          Dates: arritem.Dates,
                          Time : [
                              {TimeStart: arritem.TimeStart, TimeEnd: arritem.TimeEnd}
                          ]
                      })
                 }
              });
          }
      });
     console.log(" show",data);
      result(null, data);

    });
}

Details.update_booking = (updateDetails_booking, result) => {
  console.log(updateDetails_booking)
  sql.query(updateDetails_booking
    //   `UPDATE projectroom.Details SET BookingId = ?, status = 'W' 
    // WHERE RoomID = ?
    // and dates = ?
    // and TimeId = (Select TimeId from projectroom.Timeslot where TimeStart = ?);`,
    // [updateDetails_booking.BookingId, updateDetails_booking.RoomID,updateDetails_booking.Dates,updateDetails_booking.TimeStart]
    , (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        // not found Customer with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("update Success: ", updateDetails_booking);
      result(null, res);

    });
  // sleep(3000);
};

Details.delete = (deleteDetails, result) => {
  sql.query("DELETE FROM `projectroom`.`Booking` WHERE `BookingId` = ?", deleteDetails.BookingId, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted Success: ", deleteDetails);
    result(null, res);
  });
  // sql.query("UPDATE `projectroom`.`Details` SET `status` = 'A'  WHERE `DetailId`  = ?", updateDetails.DetailId, (err, res) => {
  //   if (err) {
  //     console.log("error: ", err);
  //     result(null, err);
  //   return;
  //   }
  //   if (res.affectedRows == 0) {
  //     // not found Customer with the id
  //     result({ kind: "not_found" }, null);
  //     return;
  //   }

  //   console.log("update Success: ", updateDetails);
  //   result(null, res);
  // });
};

Details.update = (updateDetails, result) => {
  sql.query("UPDATE `projectroom`.`Details` SET BookingId = null,`status` = 'A'  WHERE `BookingId`  = ?", updateDetails.BookingId, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("update Success: ", updateDetails);
    result(null, res);
  });
};


module.exports = Details;
