const sql = require("./db");

const Teacher = function(teacher) {
    this.TID = teacher.TID;
    this.TFname = teacher.TFname;
    this.TLname = teacher.DetailId;
    this.Position = teacher.TID
    this.Phone = teacher.Phone;
    this.email = teacher.email;
    this.password = teacher.password;
  };
Teacher.Signin = (Teacher, result) => {
    sql.query(`SELECT TID ,email, TFname,TLname FROM projectroom.Teacher Where email = ? AND password = ? `,[Teacher.email,Teacher.password], (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
      if (res.length) {
        console.log("found ");
        result(null, res[0]);
        return;
      }
      result({ kind: "not_found" }, null);
    });
  };
  
  module.exports = Teacher;
  