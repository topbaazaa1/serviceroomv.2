module.exports = app => {
   const Booking = require("../controllers/Book.controller");
  const Details = require("../controllers/Details.controller");
  const Room = require("../controllers/Room.controller");
  const Teacher = require("../controllers/Teacher.controller");


  // Sign App
  app.post("/Signin", Teacher.Signin);

  //Search Room
  app.post("/Search", Details.search)
  //Logout app

  //Booking App
  app.put("/Booking", Booking.create);
  
  //Table

  // Delete booking
  app.put("/delete", Details.delete)
  
  app.get("/SelectBuild",Room.SelectBuild)

  app.post("/SelectBooking",Booking.Select)

  app.post("/SelectTable",Room.SelectTable)

  app.post("/SelectRoom",Room.SelectRoom)
  

  
};
