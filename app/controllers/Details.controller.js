const Details = require("../models/Details.model");
var moment = require('moment');

  exports.search = (req, res) => {
    const details = new Details({
      TimeStart : req.body.TimeStart,
      TimeEnd :   req.body.TimeEnd,
      Status :    req.body.Status,
      Build :     req.body.Build,
      Capacity :  req.body.Capacity,
      Dates  :    req.body.Dates
    });
    Details.search(details,(err,data) =>{
      if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Customer."
      });
    else {
      // console.log(data[Dates])
      res.send(data);}
    })
  };

  exports.delete = (req, res) => {
    const details = new Details({
      BookingId : req.body.BookingId,
    });
    Details.update(details,(err,data) =>{
      if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Customer."
      });
    else {  
      Details.delete(details, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Details with id ${details}.`
          });
        } else {
          res.status(500).send({
            message: "Could not delete Details with id " + details
          });
        }
      } else res.send({ message: `Booking were deleted successfully!` });
    });}
    })

  };

  // exports.update_booking = (req, res) => {
  //   // Validate Request
  //   if (!req.body) {
  //     res.status(400).send({
  //       message: "Content can not be empty!"
  //     });
  //   }
  //   const details = new Details({
  //     BookingId : req.body.BookingId,
  //     DetailId : req.body.DetailId
  //   });

  //   Details.update_booking(details, (err, data) => {
  //       if (err) {
  //         if (err.kind === "not_found") {
  //           res.status(404).send({
  //             message: `Not found Details with id ${details}.`
  //           });
  //         } else {
  //           res.status(500).send({
  //             message: "Error updating Details with id " + details
  //           });
  //         }
  //       } else res.send(data);
  //     }
  //   );
  // };
  
  // exports.update = (req, res) => {
  //   const details = new Details({
  //     DetailId : req.body.DetailId
  //   });
  //   Details.update(details,(err,data) =>{
  //     if (err)
  //     res.status(500).send({
  //       message:
  //         err.message || "Some error occurred while creating the Customer."
  //     });
  //   else res.send(data);
  //   })
  // };